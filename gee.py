#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright (C) 2016 Gee
#   
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Part of the code is inspired by Trolltech AS imageviewer.py 

from PySide import QtCore, QtGui

currentPath = ""
currentFile = ""
extentions = [".png",".jpg",".gif",".jpeg",".JPG",".JPEG",".svg"]

class HistoryManager:
    def __init__(self):
        self.listdir = []
        self.assoc = {}
        self.position = -1
        self.last = ""
        self.nohisto = False

    def remove(self,i):
        h = self.listdir[i]
        self.listdir.remove(h)
        del self.assoc[h]

    def load(self):
        home = os.environ["HOME"]
        if not os.path.exists(os.path.join(home,".local/share/gee")):
            os.mkdir(os.path.join(home,".local/share/gee"))
        if os.path.exists(os.path.join(home,".local/share/gee/history.txt")):
            f = open(os.path.join(home,".local/share/gee/history.txt"),"r")
            d = f.readlines()
            for dd in d:
                sd = dd.strip().split()
                if len(sd)==2:
                    self.listdir.append(sd[0])
                    self.assoc[sd[0]] = sd[1]
            f.close()

    def save(self):
        home = os.environ["HOME"]
        if not os.path.exists(os.path.join(home,".local/share/gee")):
            os.mkdir(os.path.join(home,".local/share/gee"))
        f = open(os.path.join(home,".local/share/gee/history.txt"),"w")
        for h in self.listdir:
            hc = h.encode("utf8")
            ac = self.assoc[h].encode("utf8")
            f.write("%s %s\n"%(hc,ac))
        f.close()

    def updir(self,di):
        if self.nohisto:
            self.last = di
        else:
            self.position = -1
            if self.last:
                la = self.last
                self.last = ""
                self.updir(la)
            if di in self.listdir:
                self.listdir.remove(di)
            self.listdir.append(di)

    def changedir(self,di,fi):
        #if the file is given, show this one
        #if not:
        # if the dir contains recent changes (<5min ?), display the latest changes
        # elif there is an entry in the history, display the latest history entry
        # else display the latest changes
        if fi != "":
            self.updir(di)
            self.assoc[di] = fi
        else:
            #1) get ctime

            cPd = ""
            try:
                cPd = di.decode('unicode-escape')
            except:
                cPd = di
            onlyFiles = [ [os.path.getmtime(os.path.join(di,f)),f] for f in os.listdir(cPd) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] in extentions and not f.startswith(".")]
            if onlyFiles:
                onlyFiles.sort()
                #2) if old enough, show the one of the history
                if time.time()-onlyFiles[-1][0] > 60:
                    if di in self.listdir:
                        self.updir(di)
                        return self.assoc[di]
                #3)if not in the history, return the lastest
                return onlyFiles[-1][1]
            
        return fi

    def associate(self,di,fi):
        if fi != "":
            self.assoc[di] = fi
            if di not in self.listdir:
                self.listdir.append(di)

    def back(self):
        if self.position == -1:
            self.position = len(self.listdir)-2
        elif self.position > 0:
            self.position += -1
        v,di,fi = self.get(self.position)
        return v,di,fi

    def get(self,i):
        if i >= 0 and i <= len(self.listdir):
            di = self.listdir[i]
            fi = self.assoc[di]
            return 1,di,fi
        return 0,"",""

history = HistoryManager()

class Gee(QtGui.QMainWindow):
    def __init__(self):
        super(Gee, self).__init__()

        self.onlyDirs = []
        self.onlyFiles = []

        self.currentPixmap = None
        self.image_scale = 1.
        self.image_object = None
        self.image_mouse = [0,0]

        self.currentDialog = None

        self.main = QtGui.QWidget()
        self.setCentralWidget(self.main)

        #1) directory bar
        #self.buttonEdit = QtGui.QPushButton(u'\u270f',self.main)
        #self.buttonEdit = QtGui.QPushButton(u'\u270d',self.main)
        self.buttonEdit = QtGui.QPushButton('',self.main)
        beicon = QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/pencil-2.svg") )
        self.buttonEdit.setIcon(beicon)
        self.buttonEdit.setIconSize(QtCore.QSize(18,18))
        self.buttonEdit.setToolTip('Write the current directory')
        self.buttonEdit.resize(26,26)
        self.buttonEdit.setMaximumWidth(26)
        self.buttonEdit.clicked.connect(self.buttonEditClick)
        #self.buttonEdit.setFlat(True)
        self.buttonEdit.hide()

        self.empty1 = QtGui.QWidget()
        self.empty1.setFixedWidth(8)
        self.empty1.hide()

        self.empty2 = QtGui.QWidget()
        policy = self.empty2.sizePolicy()
        policy.setHorizontalStretch(100)
        policy.setVerticalStretch(1)
        self.empty2.setSizePolicy(policy)
        self.empty2.setFixedHeight(28)
        self.empty2.hide()

        self.dirLine = QtGui.QLineEdit(self.main)
        self.dirLine.setToolTip('Current directory')
        self.dirLine.setMaximumHeight(28)
        self.dirLine.returnPressed.connect(self.dirLinePressed)
        self.dirLine.hide()

        self.dirBtnList = []
        for i in range(10):
            self.dirBtnList.append(QtGui.QPushButton(u'/',self.main))
            self.dirBtnList[-1].setFlat(True)
            self.dirBtnList[-1].setStyleSheet("padding-left: 0px; padding-right: 0px; padding-top: 1px; padding-bottom: 1px;")
            self.dirBtnList[-1].clicked.connect(lambda i=i: self.dirBtnClick(i))
            self.dirBtnList[-1].hide()

        self.layout1 = QtGui.QHBoxLayout()
        self.layout1.setContentsMargins(2,0,0,0)
        self.layout1.setSpacing(0)
        self.layout1.addWidget(self.buttonEdit)
        self.layout1.addWidget(self.empty1)
        for i in range(10):
            self.layout1.addWidget(self.dirBtnList[i])
        self.layout1.addWidget(self.empty2)
        self.layout1.addWidget(self.dirLine)

        #2) list panel

        self.filterEdit = QtGui.QLineEdit()
        self.filterEdit.textChanged.connect(self.filterEditChanged)
        #self.buttonFilt = QtGui.QPushButton(u'\u232b',self.main)
        self.buttonFilt = QtGui.QPushButton('',self.main)
        fiicon = QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/remove-circle-1.svg") )
        self.buttonFilt.setIcon(fiicon)
        self.buttonFilt.setIconSize(QtCore.QSize(18,18))
        self.buttonFilt.setToolTip('Erase the filter box content')
        self.buttonFilt.resize(28,28)
        self.buttonFilt.setMaximumWidth(28)
        self.buttonFilt.setMaximumHeight(28)
        #self.buttonFilt.setFlat(True)
        self.buttonFilt.clicked.connect(self.buttonFiltClick)
        self.layoutp = QtGui.QHBoxLayout()
        self.layoutp.setContentsMargins(0,0,0,0)
        self.layoutp.setSpacing(0)
        self.layoutp.addWidget(self.filterEdit)
        self.layoutp.addWidget(self.buttonFilt)


        self.listView = QtGui.QListView()
        self.listView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        policy = self.listView.sizePolicy()
        policy.setHorizontalStretch(1)
        self.listView.setSizePolicy(policy)
        model = QtGui.QStandardItemModel(self.listView)
        self.listView.setModel(model)
        self.listView.clicked.connect(self.listViewClick)
        self.listView.doubleClicked.connect(self.listViewDoubleclick)
        self.listView.keyPressEvent_old = self.listView.keyPressEvent
        self.listView.keyPressEvent = self.keyPressEvent_listView

        self.layout2l = QtGui.QVBoxLayout()
        self.layout2l.setContentsMargins(0,0,0,0)
        self.layout2l.setSpacing(0)
        self.layout2l.addLayout(self.layoutp, 1)
        self.layout2l.addWidget(self.listView, 2)

        self.layout2r = QtGui.QVBoxLayout()
        self.layout2r.setContentsMargins(0,0,0,0)
        self.layout2r.setSpacing(0)
        self.buttons = []
        button_prop = {}
        button_prop["ba"] = [u'\u2b05','Back in the previous directory',0]
        button_prop["zi"] = [u'+','Zoom in',0]
        button_prop["zo"] = [u'-','Zoom out',0]
        button_prop["z0"] = [u'1','Original size',0]
        button_prop["zf"] = [u'\u2b1a','Scale to the available space',0]
        button_prop["gu"] = [u'\u271b','Show the guide',1]
        button_prop["hi"] = [u'\u2690','History',0]
        button_prop["pv"] = [u'\u25a6','Preview grid',0]
        #button_prop["co"] = [u'\u273b','Coordinate tool',0]
        #button_prop["cb"] = [u'\u2302','Show color-blind versions of the image',0]
        button_prop["nw"] = [u'\u2302','Duplicate window',0]
                   
        #self.bs = ["ba","zi","zo","z0","zf","gu","hi","pv","co","cb","nw"]
        self.bs = ["ba","zi","zo","z0","zf","gu","hi","pv","nw"]
        button_ico = []
        for i in range(len(self.bs)):
            p = button_prop[self.bs[i]]

            if os.path.exists( os.path.dirname(os.path.realpath(__file__))+"/img/"+self.bs[i]+".svg" ) :
                self.buttons.append( QtGui.QPushButton('',self.main) )
                button_ico.append( QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/"+self.bs[i]+".svg") ) )
                self.buttons[-1].setIcon(button_ico[-1])
                self.buttons[-1].setIconSize(QtCore.QSize(16,16))
            else:
                self.buttons.append( QtGui.QPushButton(p[0],self.main) )
            self.buttons[-1].setToolTip(p[1])
            self.buttons[-1].resize(24,24)
            self.buttons[-1].setFlat(True)
            self.buttons[-1].setMaximumWidth(24)
            self.buttons[-1].setMaximumHeight(24)
            if p[2] == 1:
                self.buttons[-1].setCheckable(True)
            self.buttons[-1].clicked.connect(lambda i=i: self.buttonsClick(i))
            self.layout2r.addWidget(self.buttons[-1], 1)
        self.layout2r.addStretch(100)


        self.layout2 = QtGui.QHBoxLayout()
        self.layout2.setContentsMargins(2,0,0,2)
        self.layout2.setSpacing(0)
        self.layout2.addLayout(self.layout2l, 1)
        self.layout2.addLayout(self.layout2r, 2)
        self.layout2o = QtGui.QWidget()
        self.layout2o.setLayout(self.layout2)



        #3) image display

        self.scene = QtGui.QGraphicsScene()
        self.view = QtGui.QGraphicsView(self.scene,self.main)
        self.view.setStyleSheet("background-color : gray; border:none;");
        policy = self.view.sizePolicy()
        policy.setHorizontalStretch(90)
        self.view.setSizePolicy(policy)
        self.view_mouse = ["empty",0,0]
        self.scene.mousePressEvent = self.mousePressEvent_view
        self.scene.mouseMoveEvent = self.mouseMoveEvent_view
        self.scene.mouseReleaseEvent = self.mouseReleaseEvent_view
        self.view.show()
        self.view.setMouseTracking(True)

        self.splitter = QtGui.QSplitter(self.main)
        self.splitter.addWidget(self.layout2o)
        self.splitter.addWidget(self.view)
        
        #4) putting everything together

        self.createDirBar()

        self.layout3 = QtGui.QVBoxLayout(self.main)
        self.layout3.setContentsMargins(0,0,0,0)
        self.layout3.setSpacing(0)
        self.layout3.addLayout(self.layout1, 1)
        self.layout3.addWidget(self.splitter, 2)

        self.main.setLayout(self.layout3)

        #
        self.listView.setFocus(QtCore.Qt.OtherFocusReason)

        self.createBackground()

        self.statusBar = QtGui.QLabel("loading...",self.view)
        self.statusBar.show()
        self.statusCross_show = False
        self.statusCross = []
        self.statusCrossI1 = QtGui.QPixmap(1,1000)
        self.statusCrossI1.fill(QtGui.QColor("black"))
        self.statusCrossI2 = QtGui.QPixmap(1000,1)
        self.statusCrossI2.fill(QtGui.QColor("black"))
        for c in range(4):
            self.statusCross.append(QtGui.QLabel("",self.view))
            self.statusCross[-1].hide()
            if c%2 == 0:
                self.statusCross[-1].setPixmap(self.statusCrossI1)
            else:
                self.statusCross[-1].setPixmap(self.statusCrossI2)

        self.createActions()

        self.setWindowTitle("gee.py")
        self.resizeEvent = self.resizeEvent_main
        self.resize(500, 400)
        self.setWindowState(QtCore.Qt.WindowMaximized)

    def resizeEvent_main(self,e):
        self.loadList()
        self.status()

    def status(self):
        self.statusBar.clear()
        self.statusBar.move(0,self.view.height()-18)
        if self.currentPixmap:
            self.setWindowTitle("gee.py - "+currentFile)
            t = "<html><body>"
            t += currentFile
            t += "   "+str(self.currentPixmap.width())+"x"+str(self.currentPixmap.height())
            t += "   x%.3f"%(self.image_scale)
            vx,vy = self.image_mouse[0],self.image_mouse[1]
            x,y = 0,0
            if self.image_scale > 0:
                x,y = int(round(vx*1./self.image_scale)),int(round(vy*1./self.image_scale))
            t += "   (%04i,%04i)"%(x,y)
            rgb = self.currentPixmap.toImage().pixel(x,y)
            vr,vg,vb = QtGui.qRed(rgb), QtGui.qGreen(rgb), QtGui.qBlue(rgb)
            color = QtGui.QColor(vr,vg,vb,255)
            t += "  <font color="+color.name()+"> &#x25A0; </font> [%03i,%03i,%03i]"%(vr,vg,vb)

            t += "</body></html>"
            self.statusBar.setText(t)

            for c in range(4):
                if self.statusCross_show == False:
                    self.statusCross[c].hide()
                else:
                    self.statusCross[c].show()
                    cx,cy = self.image_object.x()+vx,self.image_object.y()+vy
                    if c == 0:
                        cy += -10-self.statusCrossI1.height()
                    elif c == 1:
                        cx += -10-self.statusCrossI2.width()
                    elif c == 2:
                        cy += 10
                    elif c == 3:
                        cx += 10
                    self.statusCross[c].move(cx,cy)
                    self.statusCross[c].adjustSize()
                    self.statusCross[c].raise_()

        self.statusBar.adjustSize()
        self.statusBar.raise_()

    def createBackground(self):
        image = QtGui.QImage(os.path.dirname(os.path.realpath(__file__))+"/img/bkgd.png")
        self.background = QtGui.QPixmap.fromImage(image)
        
        a1,a2 = self.background.width(),self.background.height()
        b1,b2 = self.width(),self.height()
        self.background_object = []
        #TODO: not working, custom value
        for x in range(7):
            for y in range(5):
                self.background_object.append(QtGui.QLabel("",self.view))
                self.background_object[-1].mousePressEvent = self.mousePressEvent_view
                self.background_object[-1].mouseMoveEvent = self.mouseMoveEvent_view
                self.background_object[-1].mouseReleaseEvent = self.mouseReleaseEvent_view
                self.background_object[-1].setMouseTracking(True)


                self.background_object[-1].setPixmap( self.background )
                self.background_object[-1].move(a1*x,a2*y)
                self.background_object[-1].setScaledContents(True)

    def mousePressEvent_view(self,mouse):

        if mouse.button() == QtCore.Qt.LeftButton:
            try:
                self.view_mouse = ["press",mouse.globalPos().x()+0,mouse.globalPos().y()+0]
            except:
                self.view_mouse = ["press",mouse.scenePos().x()+0,mouse.scenePos().y()+0]
            self.status()

    def mouseReleaseEvent_view(self,mouse):
        self.view_mouse[0] = ["empty"]

    def mouseMoveEvent_image(self,mouse):
        self.image_mouse = [mouse.pos().x(),mouse.pos().y()]
        self.mouseMoveEvent_view(mouse)
        self.status()

    def mouseMoveEvent_view(self,mouse):
        nx,ny = 0,0
        try:
            nx,ny = mouse.globalPos().x()+0,mouse.globalPos().y()+0
        except:
            nx,ny = mouse.scenePos().x()+0,mouse.scenePos().y()+0
        if self.view_mouse[0] == "pmove" or self.view_mouse[0] == "press":
            px,py = self.view_mouse[1],self.view_mouse[2]
            if self.image_object:
                rx = self.image_object.x()
                ry = self.image_object.y()
                dx = int(round((nx-px)*1))
                dy = int(round((ny-py)*1))
                self.image_object.move(rx+dx,ry+dy)
            self.view_mouse = ["pmove",nx,ny]
        else:
            self.view_mouse = ["move",nx,ny]


    def wheelEvent(self,mouse):
        #hm, ugly, but whatever
        #TODO: do it correctly
        x,y = mouse.globalPos().x(),mouse.globalPos().y()
        if x >= self.listView.width()+10 and y >= 75:
            if mouse.delta() > 0:
                self.zoom(1.1)
            else:
                self.zoom(0.91)

    def keyPressEvent_listView(self,t):
        self.listView.keyPressEvent_old(t)
        if t.key()==QtCore.Qt.Key_Return:
            self.listViewClick(True)
        if t.key()==QtCore.Qt.Key_Up or t.key()==QtCore.Qt.Key_Down:
            self.listViewClick(False)


    def filterEditChanged(self):
        self.refreshList(False)

    def goto(self,i,t):
        global currentPath,currentFile
        if t == "listview":
            currentFile = self.onlyFiles[i]
            self.loadImage()
            self.displayImage(True)
            #TODO: select the correct one in the list
        elif t == "history":
            currentPath = i
            currentFile = history.assoc[i]
            history.nohisto = True
            self.createDirBar()
            self.loadList()
            history.nohisto = False


    def listViewClick(self,double=False):
        global currentPath,currentFile
        ci = self.listView.currentIndex()
        text = ci.data(QtCore.Qt.DisplayRole)
        if text in self.onlyDirs and double:
            #move dir
            if text == "..":
                currentPath = "/".join(currentPath.split("/")[:-1])
                if currentPath == "":
                    currentPath = "/"
                currentFile = ""
            else:
                currentPath = os.path.join(currentPath,text)
                currentFile = ""
            self.createDirBar()
            self.loadList()
        if text in self.onlyFiles:
            #display image
            currentFile = text
            self.loadImage()
            self.displayImage(True)

    def listViewDoubleclick(self):
        self.listViewClick(True)

    def dirLinePressed(self):
        global currentPath,currentFile
        cp = self.dirLine.text()
        GetPath(cp)
        self.createDirBar()
        self.loadList()

    def closeDialog(self,evt):
        self.currentDialog = None

    def buttonsClick(self,i):
        global history,currentPath,currentFile
        if self.bs[i] == "ba":
            v,di,fi = history.back()
            if v:
                currentPath,currentFile = di,fi
                history.nohisto = True
                self.createDirBar()
                self.loadList()
                history.nohisto = False
        elif self.bs[i] == "gu":
            self.statusCross_show = self.buttons[i].isChecked()
            self.status()
        elif self.bs[i] == "zi":
            self.zoomIn()
        elif self.bs[i] == "zo":
            self.zoomOut()
        elif self.bs[i] == "z0":
            self.normalSize()
        elif self.bs[i] == "zf":
            self.fitToWindow()
        elif self.bs[i] in ["pv","hi"]:
            if self.currentDialog:
                pass#a dialog is open
            else:
                if self.bs[i] == "pv":
                    self.currentDialog = PreviewDlg(self)
                if self.bs[i] == "hi":
                    self.currentDialog = HistoDlg(self)
                self.currentDialog.run()
        elif self.bs[i] == "nw":
            os.system(os.path.dirname(os.path.realpath(__file__))+"/gee.py %s &"%os.path.join(currentPath,currentFile))

    def buttonEditClick(self):
        self.createDirBar(True)

    def buttonFiltClick(self):
        self.filterEdit.clear()

    def dirBtnClick(self,i):
        global currentPath,currentFile
        sp = currentPath.split("/")
        spi = range(len(sp))
        if sp[-1] == "":
            sp = sp[:-1]
            spi = spi[:-1]
        sp = sp[-10:]
        spi = spi[-10:]
        sp = currentPath.split("/")
        currentPath = "/".join(sp[:spi[i]+1])
        currentFile = ""
        self.createDirBar()
        self.loadList()

    def createDirBar(self,edit=False):

        if edit:
            self.dirLine.show()
            self.buttonEdit.hide()
            self.empty1.hide()
            self.empty2.hide()
            for b in self.dirBtnList:
                b.hide()

            self.dirLine.clear()
            self.dirLine.insert(os.path.join(currentPath,currentFile))

        else:
            self.dirLine.hide()
            self.buttonEdit.show()
            self.empty1.show()
            self.empty2.show()

            sp = currentPath.split("/")
            if sp[-1] == "":
                sp = sp[:-1]

            lt = []
            for lsp in sp:
                lt.append(lsp+" /")

            for b in self.dirBtnList:
                b.hide()
            for i,llt in enumerate(lt[-10:]):
                self.dirBtnList[i].show()
                self.dirBtnList[i].setText(llt)

    def open(self):
        global currentPath,currentFile
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        fileName,_ = dialog.getOpenFileName(self, "Open Directory",currentPath)
        currentPath = os.path.split(fileName)[0]
        currentFile = os.path.split(fileName)[1]
        currentPath = os.path.realpath(currentPath)
        self.createDirBar()
        self.loadList()

    def loadImage(self):
        global currentPath,currentFile,history
        fileName = os.path.join(currentPath,currentFile)
        if fileName:
            image = QtGui.QImage(fileName)
            if image.isNull():
                QtGui.QMessageBox.information(self, "gee.py",
                        "Cannot load %s." % fileName)
                return

            self.currentPixmap = QtGui.QPixmap.fromImage(image)
            self.image_scale = 1.
            history.associate(currentPath,currentFile)

    def displayImage(self,new=False):
        if self.currentPixmap:
            if self.image_object == None:
                self.image_object = QtGui.QLabel("",self.view)
                self.image_object.mousePressEvent = self.mousePressEvent_view
                self.image_object.mouseMoveEvent = self.mouseMoveEvent_image
                self.image_object.mouseReleaseEvent = self.mouseReleaseEvent_view
                self.image_object.setMouseTracking(True)

            self.image_object.setPixmap( self.currentPixmap )
            self.image_object.setScaledContents(True)
            if new:
                self.image_object.move(0,0)
                self.fitToWindow(True)
            self.image_object.resize(self.image_scale * self.currentPixmap.size())
            self.image_object.show()
        self.status()

    def loadList(self):
        global history,currentPath,currentFile
        if currentPath == "":
            currentPath = "/"
        try:
            cPd = currentPath.decode('unicode-escape')
        except:
            cPd = ""
            for letters in currentPath:
                cPd += letters.encode('utf8')
        self.onlyFiles = [ f for f in os.listdir(cPd) if os.path.isfile(os.path.join(cPd,f)) and os.path.splitext(f)[1] in extentions and not f.startswith(".")]            
        self.onlyDirs = [ f for f in os.listdir(cPd) if os.path.isdir(os.path.join(cPd,f)) and not f.startswith(".")]
        self.onlyFiles.sort()
        self.onlyDirs.sort()
        if currentPath != "/":
            self.onlyDirs = [".."]+self.onlyDirs

        currentFile = history.changedir(currentPath,currentFile)

        self.refreshList()

    def refreshList(self,update=True):
        global currentFile
        filt = self.filterEdit.text()
        filt = filt.split("*")

        model = self.listView.model()
        model.removeRows(0,model.rowCount())
        style = self.buttonEdit.style()
        
        selected = -1
        current = -1

        curfont = None

        #1) fill the dir:
        for od in self.onlyDirs:
            if [f for f in filt if f not in od]:#.decode('unicode-escape')]:
                continue        
            current += 1
            item = QtGui.QStandardItem()
            if curfont == None:
                curfont = item.font()
                curfont.setPixelSize(10)
            item.setFont(curfont)
            item.setText(od)
            if od == currentFile:
                selected = current
            item.setIcon(style.standardIcon(QtGui.QStyle.SP_DirIcon))
            model.appendRow(item)
        #2) fill the file:
        for od in self.onlyFiles:
            if [f for f in filt if f not in od]:
                continue                    
            current += 1
            item = QtGui.QStandardItem()
            if curfont == None:
                curfont = item.font()
                curfont.setPixelSize(10)
            item.setFont(curfont)
            item.setText(od)
            if od == currentFile:
                selected = current
            item.setIcon(style.standardIcon(QtGui.QStyle.SP_FileIcon))
            model.appendRow(item)

        if selected != -1 and update:
            ci = model.createIndex(selected,0)
            self.listView.selectionModel().select( ci, QtGui.QItemSelectionModel.Select )
            self.loadImage()
            self.displayImage(True)

        self.listView.show()

    def zoom(self,sc):
        if self.currentPixmap and self.image_object:
            a1,a2 = self.currentPixmap.width(),self.currentPixmap.height()
            b1,b2 = self.view.width(),self.view.height()
            #conserve the center
            cx,cy = b1*0.5,b2*0.5
            ok = 0
            if sc>1 and min(a1*self.image_scale*1./b1,a2*self.image_scale*1./b2) < 2.5:
                ok = 1
            elif sc<1 and min(a1*self.image_scale,a2*self.image_scale) > 3:
                ok = 2
            else:
                ok = -1 if sc>1 else -2
                #too big (-1) or too small (-2)
            if ok > 0:
                x,y = self.image_object.x(),self.image_object.y()
                dx,dy = int((cx-x)*(1-sc)),int((cy-y)*(1-sc))
                self.image_object.move(x+dx,y+dy)
                self.image_scale *= sc
                self.displayImage()


    def zoomIn(self):
        self.zoom(1.25)

    def zoomOut(self):
        self.zoom(0.8)

    def normalSize(self):
        if self.image_object:
            self.image_object.adjustSize()
            self.image_scale = 1.0
            self.displayImage()

    def fitToWindow(self,onlyIfLarger=False,refresh=True):
        if self.currentPixmap and self.image_object:
            a1,a2 = self.currentPixmap.width(),self.currentPixmap.height()
            b1,b2 = self.view.width(),self.view.height()
            if (onlyIfLarger==False) or (a1 > b1 or a2 > b2):
                if b1*1./a1 < b2*1./a2:
                    self.image_scale = b1*1./a1
                else:
                    self.image_scale = b2*1./a2
                if refresh:
                    self.displayImage()                    
            a1,a2 = a1*self.image_scale,a2*self.image_scale
            self.image_object.move((b1-a1)/2,(b2-a2)/2)


    def about(self):
        QtGui.QMessageBox.about(self, "About Image Viewer",
                "<p><b>gee.py</b> is based on the Trolltech example <b>Image Viewer</b>.</p>"
                "<p>It's just a basic image viewer.</p>")

    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self, shortcut="Ctrl+O",
                triggered=self.open)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.zoomInAct = QtGui.QAction("Zoom &In (25%)", self,
                shortcut="Ctrl++", triggered=self.zoomIn)

        self.zoomOutAct = QtGui.QAction("Zoom &Out (25%)", self,
                shortcut="Ctrl+-", triggered=self.zoomOut)

        self.normalSizeAct = QtGui.QAction("&Normal Size", self,
                shortcut="Ctrl+S", triggered=self.normalSize)

        self.fitToWindowAct = QtGui.QAction("&Fit to Window", self,
                shortcut="Ctrl+F", triggered=self.fitToWindow)

        self.aboutAct = QtGui.QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QtGui.QAction("About &Qt", self,
                triggered=QtGui.qApp.aboutQt)


    def closeEvent(self, event):
        global history
        history.save()
        event.accept() # let the window close

def GetCurrentPath(cp=-1):
    global currentPath,currentFile
    currentPath = os.getcwd()
    currentFile = ""
    if len(sys.argv) > 1:
        cp = sys.argv[1]
        GetPath(cp)


def GetPath(cp):
    global currentPath,currentFile
    if not os.path.isdir(cp):
        if os.path.exists(cp):
            if os.path.isfile(cp) and os.path.splitext(cp)[1] in extentions:
                currentPath,currentFile = os.path.split(cp)
            else:
                currentPath,currentFile = os.path.split(cp)
                currentFile = ""
        else:
            cp,cf = os.path.split(cp)
            if os.path.isdir(cp):
                currentPath = cp
                for e in extentions:
                    if os.path.isfile(os.path.join(cp,cf+e)):
                        currentFile = cf+e
                    if os.path.isfile(os.path.join(cp,cf+e[1:])):
                        currentFile = cf+e[1:]
    else:
        currentPath = cp
    currentPath = os.path.realpath(currentPath)

#different tools

class HistoDlg:
    def __init__(self,main):
        self.main = main
        self.window = QtGui.QDialog(main)
        self.window.show()
        self.window.setWindowTitle("gee.py - History")

        self.type_ = "hi"

        self.layout1 = QtGui.QVBoxLayout()
        self.scrollArea = QtGui.QScrollArea(self.window)
        self.scrollArea.setLayout(self.layout1)
        self.scrollArea.show()

        self.sizer = QtGui.QWidget()
        self.layout1.addWidget(self.sizer)
        self.layout2 = QtGui.QVBoxLayout()
        self.layout2.setContentsMargins(0,0,0,0)
        self.layout2.setSpacing(0)
        self.sizer.setLayout(self.layout2)
        self.sizer.setSizePolicy(QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Fixed)
        self.scrollArea.setWidget(self.sizer)
        self.scrollArea.setWidgetResizable(True)
        self.sizer.show()

        self.labels = []
        self.others = []
        self.first = 0

        self.scrollArea.resizeEvent_old = self.scrollArea.resizeEvent
        self.scrollArea.resizeEvent = self.resize
        
        self.window.setLayout(QtGui.QGridLayout())
        self.window.layout().addWidget(self.scrollArea)
        self.window.layout().setContentsMargins(0,0,0,0)
        self.window.layout().setSpacing(0)
        self.window.resize(850, 300)
        self.window.closeEvent = self.main.closeDialog

        self.CreateList()

    def run(self):
        self.window.exec_()

    def Remove(self,i,refresh=True):
        global history
        history.remove(i)
        if refresh:
            self.CreateList()

    def Removebelow(self,i):
        global history
        ld = history.listdir
        while len(history.listdir) > i:
            self.Remove(0)
        self.Remove(0,True)

    def CreateList(self):
        global history
        ld = history.listdir
        la = history.assoc

        for l in self.labels:
            l.hide()
        for l in self.others:
            l.hide()

        icon1 = QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/remove-circle-1_bot.svg") )
        icon2 = QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/remove-circle-1_lef.svg") )        

        for i in range(len(ld)):
            j = len(ld)-1-i
            if i >= len(self.labels):
                self.labels.append(QtGui.QPushButton(self.window))
                policy = self.labels[-1].sizePolicy()
                policy.setHorizontalStretch(10)
                policy.setVerticalStretch(1)
                self.labels[-1].setSizePolicy(policy)
                self.labels[-1].resize(770,28)
                self.labels[-1].setMaximumHeight(28)
                self.labels[-1].setMinimumHeight(28)
                self.labels[-1].setStyleSheet("text-align:left;")
                self.labels[-1].clicked.connect(lambda j=j: self.main.goto(ld[j],"history"))
                hl = QtGui.QHBoxLayout()
                hl.setContentsMargins(0,0,0,0)
                hl.setSpacing(0)
                hl.addWidget(self.labels[-1],1)
                #self.others.append(QtGui.QPushButton(u'\u232b',self.window))
                self.others.append(QtGui.QPushButton('',self.window))
                self.others[-1].setIcon(icon2)
                self.others[-1].setIconSize(QtCore.QSize(18,18))
                self.others[-1].setToolTip('Erase this entry from the history')
                self.others[-1].clicked.connect(lambda j=j: self.Remove(j))
                self.others[-1].setMaximumWidth(28)
                self.others[-1].setMinimumWidth(28)
                self.others[-1].setMaximumHeight(28)
                self.others[-1].setMinimumHeight(28)
                hl.addWidget(self.others[-1],2)
                #self.others.append(QtGui.QPushButton(u'\u27cf',self.window))
                #self.others.append(QtGui.QPushButton(u'\u234c',self.window))
                self.others.append(QtGui.QPushButton('',self.window))
                self.others[-1].setIcon(icon1)
                self.others[-1].setIconSize(QtCore.QSize(18,18))
                self.others[-1].setToolTip('Erase all entries above from the history')
                self.others[-1].clicked.connect(lambda j=j: self.Removebelow(j))
                self.others[-1].setMaximumWidth(28)
                self.others[-1].setMinimumWidth(28)
                self.others[-1].setMaximumHeight(28)
                self.others[-1].setMinimumHeight(28)
                hl.addWidget(self.others[-1],3)
                self.layout2.addLayout(hl)

            self.labels[i].show()
            self.others[i*2].show()
            self.others[(i*2)+1].show()


        self.layout2.addStretch(100)
        self.resize(QtGui.QResizeEvent(QtCore.QSize(),QtCore.QSize()))

    def resize(self,evt):
        global history
        ld = history.listdir
        la = history.assoc

        we = 0
        if self.first <= 1:
            self.first += 1
            we = 300

        for i in range(len(ld)):
            j = len(ld)-1-i
            text = os.path.join(ld[j],la[ld[j]])
            metrics = QtGui.QFontMetrics(self.labels[i].font())
            elidedText = metrics.elidedText(text, QtCore.Qt.TextElideMode.ElideLeft, self.labels[-1].width()-50+we)
            self.labels[i].setText(elidedText)
        self.scrollArea.resizeEvent_old(evt)


class PreviewDlg:
    def __init__(self,main):
        self.main = main
        self.window = QtGui.QDialog(main)
        self.window.show()
        self.window.setWindowTitle("gee.py - Preview")

        self.type_ = "pv"

        self.s_w = 200
        self.s_h = 100
        self.s_col = 4

        self.sizer = QtGui.QWidget(self.window)
        self.scrollArea = QtGui.QScrollArea(self.window)
        self.scrollArea.setStyleSheet( "border:0px;")
        self.scrollArea.setWidget(self.sizer)
        self.sizer.show()
        self.scrollArea.show()
        self.window.setLayout(QtGui.QGridLayout())
        self.window.layout().setContentsMargins(0,0,0,0)
        self.window.layout().setSpacing(0)
        self.window.layout().addWidget(self.scrollArea)
        self.window.resize(845, 300)
        self.window.closeEvent = self.main.closeDialog

        self.scrollArea.resizeEvent_old = self.scrollArea.resizeEvent
        self.scrollArea.resizeEvent = self.resize

        self.pixmap_loaded = {}
        self.labels = []
        self.buttons = []

        self.CreateGrid()

    def resize(self,evt):
        #change s_col based on s_w
        w = self.scrollArea.width()-20
        self.s_col =  w/self.s_w
        if self.s_col <= 0:
            self.s_col = 1
        self.CreateGrid()
        self.scrollArea.resizeEvent_old(evt)

    def run(self):
        self.window.exec_()

    def load_image(self,path):
        if path not in self.pixmap_loaded.keys():
            image_o = QtGui.QImage(path)
            cp = None
            if not image_o.isNull():
                image = image_o.scaled(QtCore.QSize(self.s_w-4,self.s_h-4), QtCore.Qt.KeepAspectRatio , QtCore.Qt.SmoothTransformation )
                cp = QtGui.QPixmap.fromImage(image)
            self.pixmap_loaded[path] = cp
        return self.pixmap_loaded[path]

    def CreateGrid(self):
        lf = self.main.onlyFiles
        tw,th = len(lf),1+(len(lf)/self.s_col)
        if len(lf) > self.s_col:
            tw = self.s_col

        w,h = tw*self.s_w,th*self.s_h
        self.sizer.resize(w,h)
        self.sizer.setMaximumWidth(w)
        self.sizer.setMinimumWidth(w)
        self.sizer.setMaximumHeight(h)
        self.sizer.setMinimumHeight(h)

        for i in range(len(lf)):

            x,y = self.s_w*(i%self.s_col),self.s_h*(i/self.s_col)

            if i >= len(self.labels):
                self.labels.append(QtGui.QLabel("",self.sizer))
                cp = self.load_image(os.path.join(currentPath,lf[i]))
                if cp:
                    self.labels[i].setPixmap(cp)
                    self.labels[i].adjustSize()

                self.labels[i].setMaximumWidth(cp.width())
                self.labels[i].setMinimumWidth(cp.width())
                self.labels[i].setMaximumHeight(cp.height())
                self.labels[i].setMinimumHeight(cp.height())

            dx = (self.s_w-4-self.labels[i].pixmap().width())/2
            dy = (self.s_h-4-self.labels[i].pixmap().height())/2
            self.labels[i].move(x+2+dx,y+2+dy)
            self.labels[i].show()

            if i >= len(self.buttons):
                self.buttons.append(QtGui.QPushButton(lf[i],self.sizer))
                self.buttons[i].setMaximumWidth(self.s_w-4)
                self.buttons[i].setMinimumWidth(self.s_w-4)
                self.buttons[i].setMaximumHeight(20)
                self.buttons[i].setMinimumHeight(20)
                self.buttons[i].setStyleSheet( "background-color: rgba( 0, 0, 0, 50% ); color: rgba(255,255,255,100%);" )
                metrics = QtGui.QFontMetrics(self.buttons[i].font())
                elidedText = metrics.elidedText(lf[i], QtCore.Qt.TextElideMode.ElideRight, self.buttons[-1].width())
                self.buttons[i].setText(elidedText)
                self.buttons[i].clicked.connect(lambda i=i: self.main.goto(i,"listview"))
            self.buttons[i].move(x+2,y)
            self.buttons[i].show()


if __name__ == '__main__':

    import sys
    import os
    import time

    history.load()
    GetCurrentPath()

    app = QtGui.QApplication(sys.argv)
    imageViewer = Gee()
    imageViewer.show()
    sys.exit(app.exec_())

